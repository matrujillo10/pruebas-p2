# Parcial 2 pruebas

> Cabe resaltar que el apk baseline ejecutó las pruebas sin ningún problema.

## Integrantes

- Rafael Alexander Bermúdez Riveros, 200310592
- Miller Andrés Trujillo Achury, 201517402

# Link video explicación

[Explicación casos de prueba y desarrollo del parcial](https://youtu.be/SpvwlL8fT7A)

# Mutantes encontrados

## Encontramos 241 de 615 que alcanzamos a probar.

# Los reportes

[Los reportes](https://gitlab.com/matrujillo10/pruebas-p2/blob/1c79e1600975ec912acb8b06072c4cf99989d2d3/reports.zip) 
