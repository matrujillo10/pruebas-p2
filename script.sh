#!/bin/bash
packageName="com.evancharlton.mileage-mutant"

for i in {1..4548}
do
    mutantNumber=$( printf '%d' $i)
    file="$packageName$mutantNumber/com.evancharlton.mileage_3110-aligned-debugSigned.apk"
    folder=$packageName$mutantNumber
    calabash-android resign $file 
    mkdir reports/$folder   
            
    for j in {1..2}
    do
        echo "Installing file: "$file
        adb install $file
        testNumber=$( printf '%d' $j)        
        adb shell am instrument -w -e class net.cmoaciopm.androidtest.Test$testNumber net.cmoaciopm.androidtest/android.support.test.runner.AndroidJUnitRunner  > reports/$folder/test$testNumber.txt    
        adb uninstall "com.evancharlton.mileage"
    done    
done
