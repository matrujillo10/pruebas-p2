#!/bin/bash

packageName="com.evancharlton.mileage-mutant"
passedTests=0
testedApks=277

for ((i=1;i<=testedApks; i++))
do
    mutantNumber=$( printf '%d' $i)    
    folder=$packageName$mutantNumber        
    passedTest=true

    for j in {1..2}
    do
        testNumber=$( printf '%d' $j)          
        if grep -Fxq "OK (1 test)" reports/$folder/test$testNumber.txt
        then
           :
        else            
            passedTest=false            
        fi 
    done    
    
    if $passedTest 
    then
        passedTests=$((passedTests+1))                    
    fi

done

mutants=$((testedApks-passedTests))

echo "Mutants found $mutants of $testedApks"
