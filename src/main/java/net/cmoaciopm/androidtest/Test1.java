package net.cmoaciopm.androidtest;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class Test1 {

    private static final String ACTIVITY_TO_BE_TESTED = "com.evancharlton.mileage.Mileage";

    @Before
    public void setup() {
        Class activityClass;
        try {
            activityClass = Class.forName(ACTIVITY_TO_BE_TESTED);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        instrumentation.setInTouchMode(true);

        final String targetPackage = instrumentation.getTargetContext().getPackageName();
        Intent startIntent = new Intent(Intent.ACTION_MAIN);
        startIntent.setClassName(targetPackage, activityClass.getName());
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        instrumentation.startActivitySync(startIntent);
        instrumentation.waitForIdleSync();
    }

    @Test
    public void mileageTest() {
        ViewInteraction imageView = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        0),
                                0),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction textView = onView(
                allOf(withId(android.R.id.title), withText("Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        0),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("Fillup")));

        ViewInteraction imageView2 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        1),
                                0),
                        isDisplayed()));
        imageView2.check(matches(isDisplayed()));

        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.title), withText("History"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        1),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("History")));

        ViewInteraction imageView3 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        2),
                                0),
                        isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction textView3 = onView(
                allOf(withId(android.R.id.title), withText("Statistics"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        2),
                                1),
                        isDisplayed()));
        textView3.check(matches(withText("Statistics")));

        ViewInteraction imageView4 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        3),
                                0),
                        isDisplayed()));
        imageView4.check(matches(isDisplayed()));

        ViewInteraction textView4 = onView(
                allOf(withId(android.R.id.title), withText("Vehicles"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        3),
                                1),
                        isDisplayed()));
        textView4.check(matches(withText("Vehicles")));

        ViewInteraction editText = onView(
                allOf(withId(getId("price")), withHint("Price per Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        0),
                                0),
                        isDisplayed()));
        editText.check(matches(withHint("Price per Gallons")));

        ViewInteraction editText2 = onView(
                allOf(withId(getId("volume")), withHint("Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        0),
                                1),
                        isDisplayed()));
        editText2.check(matches(withHint("Gallons")));

        ViewInteraction editText3 = onView(
                allOf(withId(getId("odometer")), withHint("Odometer"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        1),
                                0),
                        isDisplayed()));
        editText3.check(matches(withHint("Odometer")));

        ViewInteraction button = onView(withId(getId("date")));
        button.check(matches(isDisplayed()));

        ViewInteraction checkBox = onView(withId(getId("partial")));
        checkBox.check(matches(isDisplayed()));
        checkBox.check(matches(isNotChecked()));
        checkBox.check(matches(withText("Tank was not filled to the top")));

        ViewInteraction header1 = onView(withText("Fillup information"));
        header1.check(matches(isDisplayed()));

        ViewInteraction header2 = onView(withText("Extra data"));
        header2.check(matches(isDisplayed()));

        ViewInteraction editText4 = onView(allOf(withHint("Comment"), isDisplayed()));
        editText4.check(matches(withText("")));

        ViewInteraction button2 = onView(
                allOf(withId(getId("save_btn")),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button2.check(matches(withText("Save Fillup")));

        ViewInteraction editText5 = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText5.perform(scrollTo(), replaceText("3"));

        ViewInteraction editText6 = onView(
                allOf(withId(getId("volume")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                1)));
        editText6.perform(scrollTo(), replaceText("10"));

        ViewInteraction editText7 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                0)));
        editText7.perform(scrollTo(), replaceText("100"));

        ViewInteraction fieldView = onView(
                childAtPosition(
                        allOf(withId(getId("container")),
                                childAtPosition(
                                        withId(getId("contents")),
                                        4)),
                        1));
        fieldView.perform(scrollTo(), replaceText("Tanqueé en x lugar"));

        // changeDatePicker();

        ViewInteraction button3 = onView(
                allOf(withId(getId("save_btn")), withText("Save Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button3.perform(click());

        ViewInteraction textView8 = onView(
                allOf(withId(getId("volume")), withText("10.00 g"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        1),
                                0),
                        isDisplayed()));
        textView8.check(matches(withText("10.00 g")));

        ViewInteraction textView9 = onView(
                allOf(withId(getId("price")), withText("3.00"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        1),
                                1),
                        isDisplayed()));
        textView9.check(matches(withText("3.00")));

        ViewInteraction imageView5 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        0),
                                0),
                        isDisplayed()));
        imageView5.check(matches(isDisplayed()));

        ViewInteraction textView10 = onView(
                allOf(withId(android.R.id.title), withText("Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        0),
                                1),
                        isDisplayed()));
        textView10.check(matches(withText("Fillup")));

        ViewInteraction imageView6 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        1),
                                0),
                        isDisplayed()));
        imageView6.check(matches(isDisplayed()));

        ViewInteraction textView11 = onView(
                allOf(withId(android.R.id.title), withText("History"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        1),
                                1),
                        isDisplayed()));
        textView11.check(matches(withText("History")));

        ViewInteraction imageView7 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        2),
                                0),
                        isDisplayed()));
        imageView7.check(matches(isDisplayed()));

        ViewInteraction textView12 = onView(
                allOf(withId(android.R.id.title), withText("Statistics"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        2),
                                1),
                        isDisplayed()));
        textView12.check(matches(withText("Statistics")));

        ViewInteraction imageView8 = onView(
                allOf(withId(android.R.id.icon),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        3),
                                0),
                        isDisplayed()));
        imageView8.check(matches(isDisplayed()));

        ViewInteraction textView13 = onView(
                allOf(withId(android.R.id.title), withText("Vehicles"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.tabs),
                                        3),
                                1),
                        isDisplayed()));
        textView13.check(matches(withText("Vehicles")));

        DataInteraction linearLayout = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)))
                .atPosition(0);
        linearLayout.perform(click());

        ViewInteraction textView14 = onView(
                allOf(withId(android.R.id.title), withText(containsString("Fillup on")), isDisplayed()));
        textView14.check(matches(isDisplayed()));

        ViewInteraction button4 = onView(
                allOf(withId(getId("next")),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0),
                                0),
                        isDisplayed()));
        button4.check(matches(isDisplayed()));

        ViewInteraction button5 = onView(
                allOf(withId(getId("previous")),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0),
                                1),
                        isDisplayed()));
        button5.check(matches(isDisplayed()));


        ViewInteraction textView16 = onView(
                allOf(withId(android.R.id.text1), withText("Fillup information"),
                        childAtPosition(
                                allOf(withId(getId("stat_container")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                0),
                        isDisplayed()));
        textView16.check(matches(withText("Fillup information")));

        ViewInteraction textView17 = onView(
                allOf(withId(android.R.id.text1), withText("Is partial?"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        1),
                                0),
                        isDisplayed()));
        textView17.check(matches(withText("Is partial?")));

        ViewInteraction textView18 = onView(
                allOf(withId(android.R.id.text2), withText("false"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        1),
                                1),
                        isDisplayed()));
        textView18.check(matches(withText("false")));

        ViewInteraction textView19 = onView(
                allOf(withId(android.R.id.text1), withText("Volume"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        2),
                                0),
                        isDisplayed()));
        textView19.check(matches(withText("Volume")));

        ViewInteraction textView20 = onView(
                allOf(withId(android.R.id.text2), withText("10.00 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        2),
                                1),
                        isDisplayed()));
        textView20.check(matches(withText("10.00 Gallons")));

        ViewInteraction textView21 = onView(
                allOf(withId(android.R.id.text1), withText("Odometer"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        3),
                                0),
                        isDisplayed()));
        textView21.check(matches(withText("Odometer")));

        ViewInteraction textView22 = onView(
                allOf(withId(android.R.id.text2), withText("100.00 mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        3),
                                1),
                        isDisplayed()));
        textView22.check(matches(withText("100.00 mi")));

        ViewInteraction textView23 = onView(
                allOf(withId(android.R.id.text1), withText("Price per unit"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        4),
                                0),
                        isDisplayed()));
        textView23.check(matches(withText("Price per unit")));

        ViewInteraction textView24 = onView(
                allOf(withId(android.R.id.text2), withText("$3.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        4),
                                1),
                        isDisplayed()));
        textView24.check(matches(withText("$3.00")));

        ViewInteraction textView25 = onView(
                allOf(withId(android.R.id.text1), withText("Total cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        5),
                                0),
                        isDisplayed()));
        textView25.check(matches(withText("Total cost")));

        ViewInteraction textView26 = onView(
                allOf(withId(android.R.id.text2), withText("$30.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        5),
                                1),
                        isDisplayed()));
        textView26.check(matches(withText("$30.00")));

        ViewInteraction textView27 = onView(
                allOf(withId(android.R.id.text1), withText("Statistics"),
                        childAtPosition(
                                allOf(withId(getId("stat_container")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                6),
                        isDisplayed()));
        textView27.check(matches(withText("Statistics")));

        ViewInteraction textView28 = onView(
                allOf(withId(android.R.id.text1), withText("Distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        7),
                                0),
                        isDisplayed()));
        textView28.check(matches(withText("Distance")));

        ViewInteraction textView29 = onView(
                allOf(withId(android.R.id.text1), withText("Economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        8),
                                0),
                        isDisplayed()));
        textView29.check(matches(withText("Economy")));

        ViewInteraction button6 = onView(
                allOf(withId(getId("edit")),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        button6.check(matches(isDisplayed()));

        ViewInteraction button7 = onView(
                allOf(withId(getId("edit")), withText("Edit"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        button7.perform(click());

        ViewInteraction editText8 = onView(
                allOf(withId(getId("price")), withText("3.0"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText8.perform(scrollTo(), replaceText("2.89"));

        ViewInteraction editText9 = onView(
                allOf(withId(getId("price")), withText("2.89"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0),
                        isDisplayed()));
        editText9.perform(closeSoftKeyboard());

        ViewInteraction button8 = onView(
                allOf(withId(getId("save_btn")), withText("Save changes"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button8.perform(click());

        pressBack();

        ViewInteraction textView30 = onView(
                allOf(withId(getId("price")), withText("2.89"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        1),
                                1),
                        isDisplayed()));
        textView30.check(matches(withText("2.89")));

        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction textView31 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel economy"), isDisplayed()));
        textView31.check(matches(withText("Fuel economy")));

        ViewInteraction textView32 = onView(
                allOf(withId(android.R.id.text1), withText("Average fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                0),
                        isDisplayed()));
        textView32.check(matches(withText("Average fuel economy")));

        ViewInteraction textView33 = onView(
                allOf(withId(android.R.id.text1), withText("Worst fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        2),
                                0),
                        isDisplayed()));
        textView33.check(matches(withText("Worst fuel economy")));

        ViewInteraction textView34 = onView(
                allOf(withId(android.R.id.text1), withText("Best fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        3),
                                0),
                        isDisplayed()));
        textView34.check(matches(withText("Best fuel economy")));

        ViewInteraction textView35 = onView(
                allOf(withId(android.R.id.text1), withText("Distance between fillups"),
                        isDisplayed()));
        textView35.check(matches(withText("Distance between fillups")));

        ViewInteraction textView36 = onView(
                allOf(withId(android.R.id.text1), withText("Average distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        5),
                                0),
                        isDisplayed()));
        textView36.check(matches(withText("Average distance")));

        ViewInteraction textView37 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        6),
                                0),
                        isDisplayed()));
        textView37.check(matches(withText("Minimum distance")));

        ViewInteraction textView38 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        7),
                                0),
                        isDisplayed()));
        textView38.check(matches(withText("Maximum distance")));

        ViewInteraction textView39 = onView(
                allOf(withId(android.R.id.text1), withText("Fillup costs"),
                        isDisplayed()));
        textView39.check(matches(withText("Fillup costs")));

        ViewInteraction textView40 = onView(
                allOf(withId(android.R.id.text1), withText("Average cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        9),
                                0),
                        isDisplayed()));
        textView40.check(matches(withText("Average cost")));

        ViewInteraction textView41 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        10),
                                0),
                        isDisplayed()));
        textView41.check(matches(withText("Minimum cost")));

        ViewInteraction textView42 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        11),
                                0),
                        isDisplayed()));
        textView42.check(matches(withText("Maximum cost")));

        ViewInteraction textView43 = onView(
                allOf(withId(android.R.id.text1), withText("Total cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        12),
                                0),
                        isDisplayed()));
        textView43.check(matches(withText("Total cost")));

        ViewInteraction textView44 = onView(
                allOf(withId(android.R.id.text1), withText("Cost last month"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                0),
                        isDisplayed()));
        textView44.check(matches(withText("Cost last month")));

        ViewInteraction textView45 = onView(
                allOf(withId(android.R.id.text1), withText("Estimated cost per month"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        14),
                                0),
                        isDisplayed()));
        textView45.check(matches(withText("Estimated cost per month")));

        ViewInteraction textView46 = onView(
                allOf(withId(android.R.id.text1), withText("Cost last year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                0),
                        isDisplayed()));
        textView46.check(matches(withText("Cost last year")));

        ViewInteraction textView47 = onView(
                allOf(withId(android.R.id.text1), withText("Estimated cost per year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                0),
                        isDisplayed()));
        textView47.check(matches(withText("Estimated cost per year")));

        ViewInteraction textView48 = onView(
                allOf(withId(android.R.id.text1), withText("Cost per distance"),
                        isDisplayed()));
        textView48.check(matches(withText("Cost per distance")));

        ViewInteraction textView49 = onView(
                allOf(withId(android.R.id.text1), withText("Average cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                0),
                        isDisplayed()));
        textView49.check(matches(withText("Average cost per mi")));

        DataInteraction linearLayout2 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(getId("container")),
                                1)))
                .atPosition(30);
        linearLayout2.perform(click());

        ViewInteraction textView50 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        8),
                                0),
                        isDisplayed()));
        textView50.check(matches(withText("Minimum cost per mi")));

        ViewInteraction textView51 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        9),
                                0),
                        isDisplayed()));
        textView51.check(matches(withText("Maximum cost per mi")));

        ViewInteraction textView52 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel price"),
                        isDisplayed()));
        textView52.check(matches(withText("Fuel price")));

        ViewInteraction textView53 = onView(
                allOf(withId(android.R.id.text1), withText("Average price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        11),
                                0),
                        isDisplayed()));
        textView53.check(matches(withText("Average price")));

        ViewInteraction textView54 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        12),
                                0),
                        isDisplayed()));
        textView54.check(matches(withText("Minimum price")));

        ViewInteraction textView55 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                0)));
        textView55.check(matches(withText("Maximum price")));

        ViewInteraction textView56 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel consumption"),
                        isDisplayed()));
        textView56.check(matches(withText("Fuel consumption")));

        ViewInteraction textView57 = onView(
                allOf(withId(android.R.id.text1), withText("Smallest fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                0),
                        isDisplayed()));
        textView57.check(matches(withText("Smallest fillup")));

        ViewInteraction textView58 = onView(
                allOf(withId(android.R.id.text1), withText("Largest fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                0),
                        isDisplayed()));
        textView58.check(matches(withText("Largest fillup")));

        ViewInteraction textView59 = onView(
                allOf(withId(android.R.id.text1), withText("Average fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        17),
                                0),
                        isDisplayed()));
        textView59.check(matches(withText("Average fillup")));

        ViewInteraction textView60 = onView(
                allOf(withId(android.R.id.text1), withText("Total fuel"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                0),
                        isDisplayed()));
        textView60.check(matches(withText("Total fuel")));

        ViewInteraction textView61 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel per year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        19),
                                0),
                        isDisplayed()));
        textView61.check(matches(withText("Fuel per year")));

        ViewInteraction textView62 = onView(
                allOf(withId(android.R.id.text1), withText("Fillup locations"),
                        isDisplayed()));
        textView62.check(matches(withText("Fillup locations")));

        ViewInteraction textView63 = onView(
                allOf(withId(android.R.id.text1), withText("North"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        21),
                                0),
                        isDisplayed()));
        textView63.check(matches(withText("North")));

        ViewInteraction textView64 = onView(
                allOf(withId(android.R.id.text1), withText("South"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        22),
                                0),
                        isDisplayed()));
        textView64.check(matches(withText("South")));

        ViewInteraction textView65 = onView(
                allOf(withId(android.R.id.text1), withText("East"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                0),
                        isDisplayed()));
        textView65.check(matches(withText("East")));

        ViewInteraction textView66 = onView(
                allOf(withId(android.R.id.text1), withText("West"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        24),
                                0),
                        isDisplayed()));
        textView66.check(matches(withText("West")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private static void changeDatePicker() {
        ViewInteraction dateButton = onView(
                allOf(withId(getId("date")), withText(matchesRegex("([0-9]{2})\\/([0-9]{1}([0-9]{2})?)\\/([0-9]{2})")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                1)));
        dateButton.perform(scrollTo(), click());

        ViewInteraction editText = onView(
                allOf(withClassName(is("android.widget.EditText")), withText(matchesRegex("[A-Z][a-z]{2}")),
                        isDisplayed()));
        editText.perform(click(), replaceText("Apr"));

        ViewInteraction editText1 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText(matchesRegex("[0-9]{2}")),
                        isDisplayed()));
        editText1.perform(click(), replaceText("03"));

        ViewInteraction editText2 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText(matchesRegex("[0-9]{4}")),
                        isDisplayed()));
        editText2.perform(click(), replaceText("1998"));


        ViewInteraction button = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                1),
                        isDisplayed()));
        button.perform(click());
    }

    public static Matcher<String> matchesRegex(final String regex) {
        return new TypeSafeMatcher<String>() {
            @Override
            public boolean matchesSafely(String item) {
                return item.matches(regex);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("matches regex=`" + regex + "`");
            }
        };
    }

    public static int getId(String id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        String packageName = targetContext.getPackageName();
        return targetContext.getResources().getIdentifier(id, "id", packageName);
    }
}

