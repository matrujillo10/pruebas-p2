package net.cmoaciopm.androidtest;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

public class Test2 {
    private static final String ACTIVITY_TO_BE_TESTED = "com.evancharlton.mileage.Mileage";

    @Before
    public void setup() {
        Class activityClass;
        try {
            activityClass = Class.forName(ACTIVITY_TO_BE_TESTED);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        instrumentation.setInTouchMode(true);

        final String targetPackage = instrumentation.getTargetContext().getPackageName();
        Intent startIntent = new Intent(Intent.ACTION_MAIN);
        startIntent.setClassName(targetPackage, activityClass.getName());
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        instrumentation.startActivitySync(startIntent);
        instrumentation.waitForIdleSync();
    }

    @Test
    public void mileageTest() throws ClassNotFoundException {
        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.FieldListActivity")));

        ViewInteraction textView = onView(
                allOf(withId(android.R.id.text1), withText("Comment"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                0),
                        isDisplayed()));
        textView.check(matches(withText("Comment")));

        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.text2), withText("Comment about your fillup."),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("Comment about your fillup.")));

        DataInteraction twoLineListItem = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.content),
                                0)))
                .atPosition(0);
        twoLineListItem.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(getId("title")), withText("Comment"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                0),
                        isDisplayed()));
        editText.check(matches(withText("Comment")));

        ViewInteraction editText2 = onView(
                allOf(withId(getId("description")), withText("Comment about your fillup."),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                1),
                        isDisplayed()));
        editText2.check(matches(withText("Comment about your fillup.")));

        ViewInteraction button = onView(
                allOf(withId(getId("save_btn")),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction editText3 = onView(
                allOf(withId(getId("title")), withText("Comment"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        editText3.perform(scrollTo(), click());

        ViewInteraction editText4 = onView(
                allOf(withId(getId("title")), withText("Comment"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        editText4.perform(scrollTo(), replaceText("Comm"));

        ViewInteraction editText5 = onView(
                allOf(withId(getId("title")), withText("Comm"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0),
                        isDisplayed()));
        editText5.perform(closeSoftKeyboard());

        ViewInteraction editText6 = onView(
                allOf(withId(getId("description")), withText("Comment about your fillup."),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        editText6.perform(scrollTo(), replaceText("Comment about your fillup"));

        ViewInteraction editText7 = onView(
                allOf(withId(getId("description")), withText("Comment about your fillup"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1),
                        isDisplayed()));
        editText7.perform(closeSoftKeyboard());

        ViewInteraction button2 = onView(
                allOf(withId(getId("save_btn")), withText("Save changes"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button2.perform(click());

        ViewInteraction textView3 = onView(
                allOf(withId(android.R.id.text1), withText("Comm"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                0),
                        isDisplayed()));
        textView3.check(matches(withText("Comm")));

        ViewInteraction textView4 = onView(
                allOf(withId(android.R.id.text2), withText("Comment about your fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                1),
                        isDisplayed()));
        textView4.check(matches(withText("Comment about your fillup")));

        pressBack();

        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        1),
                        isDisplayed()));
        relativeLayout.perform(click());

        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.SettingsActivity")));

        ViewInteraction textView5 = onView(
                allOf(withId(android.R.id.title), withText("Store location"),
                        isDisplayed()));
        textView5.check(matches(withText("Store location")));

        ViewInteraction textView6 = onView(
                allOf(withId(android.R.id.summary), withText("Your location for fillups will be stored privately"),
                        isDisplayed()));
        textView6.check(matches(withText("Your location for fillups will be stored privately")));


        ViewInteraction textView7 = onView(
                allOf(withId(android.R.id.title), withText("Fillup options"),
                        isDisplayed()));
        textView7.check(matches(withText("Fillup options")));

        ViewInteraction textView8 = onView(
                allOf(withId(android.R.id.summary), withText("Choose the most convenient way to enter data"),
                        isDisplayed()));
        textView8.check(matches(withText("Choose the most convenient way to enter data")));

        ViewInteraction textView9 = onView(
                allOf(withId(android.R.id.title), withText("Perform backups"),
                        isDisplayed()));
        textView9.check(matches(withText("Perform backups")));

        ViewInteraction textView10 = onView(
                allOf(withId(android.R.id.summary), withText("Your data will be automatically copied to external storage"),
                        isDisplayed()));
        textView10.check(matches(withText("Your data will be automatically copied to external storage")));

        ViewInteraction textView11 = onView(
                allOf(withId(android.R.id.title), withText("Change units"),
                        isDisplayed()));
        textView11.check(matches(withText("Change units")));

        ViewInteraction textView12 = onView(
                allOf(withId(android.R.id.summary), withText("How to set gallons/litres/km/etc."),
                        isDisplayed()));
        textView12.check(matches(withText("How to set gallons/litres/km/etc.")));

        ViewInteraction textView13 = onView(
                allOf(withId(android.R.id.title), withText("Enable notifications"),
                        isDisplayed()));
        textView13.check(matches(withText("Enable notifications")));

        ViewInteraction textView14 = onView(
                allOf(withId(android.R.id.summary), withText("Service interval reminders enabled"),
                        isDisplayed()));
        textView14.check(matches(withText("Service interval reminders enabled")));

        ViewInteraction textView15 = onView(
                allOf(withId(android.R.id.title), withText("Select ringtone"),
                        isDisplayed()));
        textView15.check(matches(withText("Select ringtone")));

        ViewInteraction textView16 = onView(
                allOf(withId(android.R.id.title), withText("Blink LED"),
                        isDisplayed()));
        textView16.check(matches(withText("Blink LED")));

        ViewInteraction textView17 = onView(
                allOf(withId(android.R.id.summary), withText("LED will flash for reminders"),
                        isDisplayed()));
        textView17.check(matches(withText("LED will flash for reminders")));

        ViewInteraction textView18 = onView(
                allOf(withId(android.R.id.title), withText("Vibrate"),
                        isDisplayed()));
        textView18.check(matches(withText("Vibrate")));

        ViewInteraction textView19 = onView(
                allOf(withId(android.R.id.summary), withText("Phone will vibrate for reminders"),
                        isDisplayed()));
        textView19.check(matches(withText("Phone will vibrate for reminders")));

        ViewInteraction textView20 = onView(
                allOf(withId(android.R.id.title), withText("About"),
                        isDisplayed()));
        textView20.check(matches(withText("About")));

        ViewInteraction textView21 = onView(
                allOf(withId(android.R.id.summary), withText("About Mileage 3.1.1"),
                        isDisplayed()));
        textView21.check(matches(withText("About Mileage 3.1.1")));

        pressBack();

        // ImportExportActivity
        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.ImportExportActivity")));

        ViewInteraction textView22 = onView(
                allOf(withText("If you need to get data off your device, or load data from an external source, this is where you want to be. In order to import or export data, use the \"mileage/\" folder on your device; you'll likely need to connect to your computer via USB for this."),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                0),
                        isDisplayed()));
        textView22.check(matches(withText("If you need to get data off your device, or load data from an external source, this is where you want to be. In order to import or export data, use the \"mileage/\" folder on your device; you'll likely need to connect to your computer via USB for this.")));

        ViewInteraction button3 = onView(
                allOf(withId(getId("import_button")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                1),
                        isDisplayed()));
        button3.check(matches(isDisplayed()));
        button3.check(matches(withText("Import data")));

        ViewInteraction button4 = onView(
                allOf(withId(getId("export_button")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                2),
                        isDisplayed()));
        button4.check(matches(isDisplayed()));
        button4.check(matches(withText("Export data")));

        pressBack();

        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.ServiceIntervalsListActivity")));

        ViewInteraction textView23 = onView(
                allOf(withText("You haven't set any service intervals!\nPress menu or use the buttons below to add some!"),
                        isDisplayed()));
        textView23.check(matches(withText("You haven't set any service intervals!\nPress menu or use the buttons below to add some!")));

        ViewInteraction button5 = onView(
                allOf(withId(getId("empty_add_interval")),
                        isDisplayed()));
        button5.check(matches(isDisplayed()));

        ViewInteraction button6 = onView(
                allOf(withId(getId("empty_edit_templates")),
                        isDisplayed()));
        button6.check(matches(isDisplayed()));

        ViewInteraction button7 = onView(
                allOf(withId(getId("empty_edit_templates")), withText("Edit templates"),
                        childAtPosition(
                                allOf(withId(android.R.id.empty),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                1)),
                                2),
                        isDisplayed()));
        button7.perform(click());

        ViewInteraction textView24 = onView(
                allOf(withId(android.R.id.text1), withText("Oil change (standard)"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                0),
                        isDisplayed()));
        textView24.check(matches(withText("Oil change (standard)")));

        ViewInteraction textView25 = onView(
                allOf(withId(android.R.id.text2), withText("Standard oil change"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        0),
                                1),
                        isDisplayed()));
        textView25.check(matches(withText("Standard oil change")));

        ViewInteraction textView26 = onView(
                allOf(withId(android.R.id.text1), withText("Oil change (synthetic)"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                0),
                        isDisplayed()));
        textView26.check(matches(withText("Oil change (synthetic)")));

        ViewInteraction textView27 = onView(
                allOf(withId(android.R.id.text2), withText("Synthetic oil change"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                1),
                        isDisplayed()));
        textView27.check(matches(withText("Synthetic oil change")));

        ViewInteraction textView28 = onView(
                allOf(withId(android.R.id.text1), withText("Replace air filter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        2),
                                0),
                        isDisplayed()));
        textView28.check(matches(withText("Replace air filter")));

        ViewInteraction textView29 = onView(
                allOf(withId(android.R.id.text2), withText("Replace air filter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        2),
                                1),
                        isDisplayed()));
        textView29.check(matches(withText("Replace air filter")));

        ViewInteraction textView30 = onView(
                allOf(withId(android.R.id.text1), withText("Power steering fluid"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        3),
                                0),
                        isDisplayed()));
        textView30.check(matches(withText("Power steering fluid")));

        ViewInteraction textView31 = onView(
                allOf(withId(android.R.id.text2), withText("Replace power steering fluid"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        3),
                                1),
                        isDisplayed()));
        textView31.check(matches(withText("Replace power steering fluid")));

        ViewInteraction textView32 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel filter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        4),
                                0),
                        isDisplayed()));
        textView32.check(matches(withText("Fuel filter")));

        ViewInteraction textView33 = onView(
                allOf(withId(android.R.id.text2), withText("Replace fuel filter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        4),
                                1),
                        isDisplayed()));
        textView33.check(matches(withText("Replace fuel filter")));

        ViewInteraction textView34 = onView(
                allOf(withId(android.R.id.text1), withText("Timing belt"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        5),
                                0),
                        isDisplayed()));
        textView34.check(matches(withText("Timing belt")));

        ViewInteraction textView35 = onView(
                allOf(withId(android.R.id.text2), withText("Replace timing belt"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        5),
                                1),
                        isDisplayed()));
        textView35.check(matches(withText("Replace timing belt")));

        ViewInteraction textView36 = onView(
                allOf(withId(android.R.id.text1), withText("Transmission fluid"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        6),
                                0),
                        isDisplayed()));
        textView36.check(matches(withText("Transmission fluid")));

        ViewInteraction textView37 = onView(
                allOf(withId(android.R.id.text2), withText("Replace transmission fluid"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        6),
                                1),
                        isDisplayed()));
        textView37.check(matches(withText("Replace transmission fluid")));

        pressBack();

        ViewInteraction button8 = onView(
                allOf(withId(getId("empty_add_interval")), withText("Add service interval"),
                        childAtPosition(
                                allOf(withId(android.R.id.empty),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                1)),
                                1),
                        isDisplayed()));
        button8.perform(click());

        ViewInteraction cursorSpinner = onView(
                allOf(withId(getId("vehicles")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        cursorSpinner.perform(scrollTo(), click());

        onView(allOf(withId(android.R.id.text1), withClassName(is("android.widget.CheckedTextView")))).perform(click());

        ViewInteraction cursorSpinner2 = onView(
                allOf(withId(getId("types")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        cursorSpinner2.perform(scrollTo(), click());

        onView(
                allOf(withId(android.R.id.text1), withClassName(is("android.widget.CheckedTextView")),
                        withText("Oil change (synthetic)"))).perform(click());

        ViewInteraction editText8 = onView(
                allOf(withId(getId("title")), withText("Oil change (synthetic)"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                5),
                        isDisplayed()));
        editText8.check(matches(withText("Oil change (synthetic)")));

        ViewInteraction editText9 = onView(
                allOf(withId(getId("description")), withText("Synthetic oil change"),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                                0)),
                                6),
                        isDisplayed()));
        editText9.check(matches(withText("Synthetic oil change")));

        ViewInteraction button9 = onView(
                allOf(withId(getId("save_btn")), withText("Add service interval"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button9.perform(click());

        ViewInteraction editText10 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("contents")),
                                        11),
                                0)));
        editText10.perform(scrollTo(), click());

        ViewInteraction editText11 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("contents")),
                                        11),
                                0)));
        editText11.perform(scrollTo(), replaceText("100"));

        onView(
                allOf(withId(getId("value")), withText("300"))).perform(scrollTo(), click(), click());

        ViewInteraction button10 = onView(
                allOf(withId(getId("save_btn")), withText("Add service interval"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button10.perform(click());

        ViewInteraction textView38 = onView(
                allOf(withId(android.R.id.text1), withText("Oil change (synthetic)"),
                        isDisplayed()));
        textView38.check(matches(withText("Oil change (synthetic)")));

        ViewInteraction textView39 = onView(
                allOf(withId(android.R.id.text2), withText("Synthetic oil change"),
                        isDisplayed()));
        textView39.check(matches(withText("Synthetic oil change")));



        pressBack();

        ViewInteraction relativeLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout2.perform(click());

        ViewInteraction relativeLayout3 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        0),
                        isDisplayed()));
        relativeLayout3.perform(click());

        ViewInteraction editText12 = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText12.perform(scrollTo(), click());

        ViewInteraction editText13 = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText13.perform(scrollTo(), replaceText("3"));

        ViewInteraction editText14 = onView(
                allOf(withId(getId("volume")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                1)));
        editText14.perform(scrollTo(), replaceText("25"));

        ViewInteraction editText15 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                0)));
        editText15.perform(scrollTo(), replaceText("100"));

        ViewInteraction button11 = onView(
                allOf(withId(getId("save_btn")), withText("Save Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button11.perform(click());

        ViewInteraction relativeLayout4 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout4.perform(click());

        ViewInteraction relativeLayout5 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        3),
                        isDisplayed()));
        relativeLayout5.perform(click());

        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.VehicleActivity")));

        ViewInteraction editText16 = onView(
                allOf(withId(getId("title")), withHint("Title"),
                        isDisplayed()));
        editText16.check(matches(withHint("Title")));

        ViewInteraction editText17 = onView(
                allOf(withId(getId("year")), withHint("Year"),
                        isDisplayed()));
        editText17.check(matches(withHint("Year")));

        ViewInteraction editText18 = onView(
                allOf(withId(getId("make")), withHint("Make"),
                        isDisplayed()));
        editText18.check(matches(withHint("Make")));

        ViewInteraction editText19 = onView(
                allOf(withId(getId("model")), withHint("Model"),
                        isDisplayed()));
        editText19.check(matches(withHint("Model")));

        ViewInteraction editText20 = onView(
                allOf(withId(getId("description")), withHint("Description"),
                        isDisplayed()));
        editText20.check(matches(withHint("Description")));

        ViewInteraction editText21 = onView(
                allOf(withId(getId("title")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText21.perform(scrollTo(), click());

        ViewInteraction editText22 = onView(
                allOf(withId(getId("title")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText22.perform(scrollTo(), replaceText("Pru"));

        ViewInteraction editText23 = onView(
                allOf(withId(getId("year")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                1)));
        editText23.perform(scrollTo(), replaceText("2010"));

        ViewInteraction editText24 = onView(
                allOf(withId(getId("make")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                0)));
        editText24.perform(scrollTo(), replaceText("Yyy"));

        ViewInteraction editText25 = onView(
                allOf(withId(getId("model")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                1)));
        editText25.perform(scrollTo(), replaceText("Gg"));

        ViewInteraction editText26 = onView(
                allOf(withId(getId("description")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        editText26.perform(scrollTo(), replaceText("Jlo"));

        ViewInteraction spinner = onView(
                allOf(withId(getId("distance")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                7)));
        spinner.perform(scrollTo(), click());

        onView(allOf(withId(android.R.id.text1), withText("Kilometers"))).perform(click());

        ViewInteraction editText27 = onView(
                allOf(withId(getId("currency")), withText("$"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("contents")),
                                        10),
                                1)));
        editText27.perform(scrollTo(), click());

        ViewInteraction editText28 = onView(
                allOf(withId(getId("currency")), withText("$"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("contents")),
                                        10),
                                1)));
        editText28.perform(scrollTo(), replaceText("5$"));

        ViewInteraction editText29 = onView(
                allOf(withId(getId("currency")), withText("5$"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("contents")),
                                        10),
                                1),
                        isDisplayed()));
        editText29.perform(closeSoftKeyboard());

        ViewInteraction button12 = onView(
                allOf(withId(getId("save_btn")), withText("Add new vehicle"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button12.perform(click());

        ViewInteraction relativeLayout6 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout6.perform(click());

        ViewInteraction relativeLayout7 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        1),
                        isDisplayed()));
        relativeLayout7.perform(click());

        ViewInteraction relativeLayout8 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        0),
                        isDisplayed()));
        relativeLayout8.perform(click());

        InstrumentationRegistry.getInstrumentation().startActivitySync(new Intent(InstrumentationRegistry.getTargetContext(), Class.forName("com.evancharlton.mileage.ServiceIntervalsListActivity")));

        DataInteraction twoLineListItem2 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.content),
                                0)))
                .atPosition(0);
        twoLineListItem2.perform(click());

        ViewInteraction cursorSpinner3 = onView(
                allOf(withId(getId("vehicles")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        cursorSpinner3.perform(scrollTo(), click());

        DataInteraction checkedTextView3 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        checkedTextView3.perform(click());

        ViewInteraction button13 = onView(
                allOf(withId(getId("save_btn")), withText("Save changes"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button13.perform(click());

        pressBack();

        ViewInteraction relativeLayout9 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        1),
                        isDisplayed()));
        relativeLayout9.perform(click());

        ViewInteraction relativeLayout10 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout10.perform(click());

        ViewInteraction relativeLayout11 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        0),
                        isDisplayed()));
        relativeLayout11.perform(click());

        ViewInteraction editText30 = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText30.perform(scrollTo(), click());

        ViewInteraction editText31 = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText31.perform(scrollTo(), replaceText("2"));

        ViewInteraction editText32 = onView(
                allOf(withId(getId("volume")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                1)));
        editText32.perform(scrollTo(), replaceText("4"));

        ViewInteraction editText33 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                0)));
        editText33.perform(scrollTo(), replaceText("110"));

        ViewInteraction button14 = onView(
                allOf(withId(getId("save_btn")), withText("Save Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button14.perform(click());

        ViewInteraction relativeLayout12 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        2),
                        isDisplayed()));
        relativeLayout12.perform(click());

        ViewInteraction textView40 = onView(
                allOf(withId(android.R.id.text1), withText("Average fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                0),
                        isDisplayed()));
        textView40.check(matches(withText("Average fuel economy")));

        ViewInteraction textView41 = onView(
                allOf(withId(android.R.id.text2), withText("2.50 mpg"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                1),
                        isDisplayed()));
        textView41.check(matches(withText("2.50 mpg")));

        ViewInteraction textView42 = onView(
                allOf(withId(android.R.id.text1), withText("Worst fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        2),
                                0),
                        isDisplayed()));
        textView42.check(matches(withText("Worst fuel economy")));

        ViewInteraction textView43 = onView(
                allOf(withId(android.R.id.text2), withText("2.50 mpg"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        2),
                                1),
                        isDisplayed()));
        textView43.check(matches(withText("2.50 mpg")));

        ViewInteraction textView44 = onView(
                allOf(withId(android.R.id.text1), withText("Best fuel economy"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        3),
                                0),
                        isDisplayed()));
        textView44.check(matches(withText("Best fuel economy")));

        ViewInteraction textView45 = onView(
                allOf(withId(android.R.id.text2), withText("2.50 mpg"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        3),
                                1),
                        isDisplayed()));
        textView45.check(matches(withText("2.50 mpg")));

        ViewInteraction textView46 = onView(
                allOf(withId(android.R.id.text1), withText("Average distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        5),
                                0),
                        isDisplayed()));
        textView46.check(matches(withText("Average distance")));

        ViewInteraction textView47 = onView(
                allOf(withId(android.R.id.text2), withText("10.00 mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        5),
                                1),
                        isDisplayed()));
        textView47.check(matches(withText("10.00 mi")));

        ViewInteraction textView48 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        6),
                                0),
                        isDisplayed()));
        textView48.check(matches(withText("Minimum distance")));

        ViewInteraction textView49 = onView(
                allOf(withId(android.R.id.text2), withText("10.00 mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        6),
                                1),
                        isDisplayed()));
        textView49.check(matches(withText("10.00 mi")));

        ViewInteraction textView50 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum distance"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        7),
                                0),
                        isDisplayed()));
        textView50.check(matches(withText("Maximum distance")));

        ViewInteraction textView51 = onView(
                allOf(withId(android.R.id.text2), withText("10.00 mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        7),
                                1),
                        isDisplayed()));
        textView51.check(matches(withText("10.00 mi")));

        ViewInteraction textView52 = onView(
                allOf(withId(android.R.id.text1), withText("Average cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        9),
                                0),
                        isDisplayed()));
        textView52.check(matches(withText("Average cost")));

        ViewInteraction textView53 = onView(
                allOf(withId(android.R.id.text2), withText("$41.50"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        9),
                                1),
                        isDisplayed()));
        textView53.check(matches(withText("$41.50")));

        ViewInteraction textView54 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        10),
                                0),
                        isDisplayed()));
        textView54.check(matches(withText("Minimum cost")));

        ViewInteraction textView55 = onView(
                allOf(withId(android.R.id.text2), withText("$8.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        10),
                                1),
                        isDisplayed()));
        textView55.check(matches(withText("$8.00")));

        ViewInteraction textView56 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        11),
                                0),
                        isDisplayed()));
        textView56.check(matches(withText("Maximum cost")));

        ViewInteraction textView57 = onView(
                allOf(withId(android.R.id.text2), withText("$75.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        11),
                                1),
                        isDisplayed()));
        textView57.check(matches(withText("$75.00")));

        ViewInteraction textView58 = onView(
                allOf(withId(android.R.id.text1), withText("Total cost"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        12),
                                0),
                        isDisplayed()));
        textView58.check(matches(withText("Total cost")));

        ViewInteraction textView59 = onView(
                allOf(withId(android.R.id.text2), withText("$83.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        12),
                                1),
                        isDisplayed()));
        textView59.check(matches(withText("$83.00")));

        ViewInteraction textView60 = onView(
                allOf(withId(android.R.id.text1), withText("Cost last month"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                0),
                        isDisplayed()));
        textView60.check(matches(withText("Cost last month")));

        ViewInteraction textView61 = onView(
                allOf(withId(android.R.id.text2), withText("$8.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                1),
                        isDisplayed()));
        textView61.check(matches(withText("$8.00")));

        ViewInteraction textView62 = onView(
                allOf(withId(android.R.id.text1), withText("Estimated cost per month"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        14),
                                0),
                        isDisplayed()));
        textView62.check(matches(withText("Estimated cost per month")));

        ViewInteraction textView63 = onView(
                allOf(withId(android.R.id.text2), withText("$2490.00 / mo"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        14),
                                1),
                        isDisplayed()));
        textView63.check(matches(withText("$2490.00 / mo")));

        ViewInteraction textView64 = onView(
                allOf(withId(android.R.id.text1), withText("Cost last year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                0),
                        isDisplayed()));
        textView64.check(matches(withText("Cost last year")));

        ViewInteraction textView65 = onView(
                allOf(withId(android.R.id.text2), withText("$8.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                1),
                        isDisplayed()));
        textView65.check(matches(withText("$8.00")));

        ViewInteraction textView66 = onView(
                allOf(withId(android.R.id.text1), withText("Estimated cost per year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                0),
                        isDisplayed()));
        textView66.check(matches(withText("Estimated cost per year")));

        ViewInteraction textView67 = onView(
                allOf(withId(android.R.id.text2), withText("$30295.00 / yr"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                1),
                        isDisplayed()));
        textView67.check(matches(withText("$30295.00 / yr")));

        ViewInteraction textView68 = onView(
                allOf(withId(android.R.id.text1), withText("Average cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                0),
                        isDisplayed()));
        textView68.check(matches(withText("Average cost per mi")));

        ViewInteraction textView69 = onView(
                allOf(withId(android.R.id.text2), withText("$0.80 / mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                1),
                        isDisplayed()));
        textView69.check(matches(withText("$0.80 / mi")));

        ViewInteraction textView70 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        19),
                                0),
                        isDisplayed()));
        textView70.check(matches(withText("Minimum cost per mi")));

        ViewInteraction textView71 = onView(
                allOf(withId(android.R.id.text2), withText("$0.80 / mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        19),
                                1),
                        isDisplayed()));
        textView71.check(matches(withText("$0.80 / mi")));

        ViewInteraction textView72 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum cost per mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        20),
                                0),
                        isDisplayed()));
        textView72.check(matches(withText("Maximum cost per mi")));

        ViewInteraction textView73 = onView(
                allOf(withId(android.R.id.text2), withText("$0.80 / mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        20),
                                1),
                        isDisplayed()));
        textView73.check(matches(withText("$0.80 / mi")));

        ViewInteraction textView74 = onView(
                allOf(withId(android.R.id.text1), withText("Average price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        22),
                                0),
                        isDisplayed()));
        textView74.check(matches(withText("Average price")));

        ViewInteraction textView75 = onView(
                allOf(withId(android.R.id.text2), withText("$2.50"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        22),
                                1),
                        isDisplayed()));
        textView75.check(matches(withText("$2.50")));

        ViewInteraction textView76 = onView(
                allOf(withId(android.R.id.text1), withText("Minimum price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                0),
                        isDisplayed()));
        textView76.check(matches(withText("Minimum price")));

        ViewInteraction textView77 = onView(
                allOf(withId(android.R.id.text2), withText("$2.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                1),
                        isDisplayed()));
        textView77.check(matches(withText("$2.00")));

        DataInteraction linearLayout = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(getId("container")),
                                1)))
                .atPosition(30);
        linearLayout.perform(click());

        ViewInteraction textView78 = onView(
                allOf(withId(android.R.id.text1), withText("Maximum price"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                0),
                        isDisplayed()));
        textView78.check(matches(withText("Maximum price")));

        ViewInteraction textView79 = onView(
                allOf(withId(android.R.id.text2), withText("$3.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        13),
                                1),
                        isDisplayed()));
        textView79.check(matches(withText("$3.00")));

        ViewInteraction textView80 = onView(
                allOf(withId(android.R.id.text1), withText("Smallest fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                0),
                        isDisplayed()));
        textView80.check(matches(withText("Smallest fillup")));

        ViewInteraction textView81 = onView(
                allOf(withId(android.R.id.text2), withText("4.00 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        15),
                                1),
                        isDisplayed()));
        textView81.check(matches(withText("4.00 Gallons")));

        ViewInteraction textView82 = onView(
                allOf(withId(android.R.id.text1), withText("Largest fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                0),
                        isDisplayed()));
        textView82.check(matches(withText("Largest fillup")));

        ViewInteraction textView83 = onView(
                allOf(withId(android.R.id.text2), withText("25.00 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        16),
                                1),
                        isDisplayed()));
        textView83.check(matches(withText("25.00 Gallons")));

        ViewInteraction textView84 = onView(
                allOf(withId(android.R.id.text1), withText("Average fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        17),
                                0),
                        isDisplayed()));
        textView84.check(matches(withText("Average fillup")));

        ViewInteraction textView85 = onView(
                allOf(withId(android.R.id.text2), withText("14.50 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        17),
                                1),
                        isDisplayed()));
        textView85.check(matches(withText("14.50 Gallons")));

        ViewInteraction textView86 = onView(
                allOf(withId(android.R.id.text1), withText("Total fuel"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                0),
                        isDisplayed()));
        textView86.check(matches(withText("Total fuel")));

        ViewInteraction textView87 = onView(
                allOf(withId(android.R.id.text2), withText("29.00 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        18),
                                1),
                        isDisplayed()));
        textView87.check(matches(withText("29.00 Gallons")));

        ViewInteraction textView88 = onView(
                allOf(withId(android.R.id.text1), withText("Fuel per year"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        19),
                                0),
                        isDisplayed()));
        textView88.check(matches(withText("Fuel per year")));

        ViewInteraction textView89 = onView(
                allOf(withId(android.R.id.text2), withText("10585.00 Gallons / yr"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        19),
                                1),
                        isDisplayed()));
        textView89.check(matches(withText("10585.00 Gallons / yr")));

        ViewInteraction textView90 = onView(
                allOf(withId(android.R.id.text1), withText("North"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        21),
                                0),
                        isDisplayed()));
        textView90.check(matches(withText("North")));

        ViewInteraction textView91 = onView(
                allOf(withId(android.R.id.text2), withText("-10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        21),
                                1),
                        isDisplayed()));
        textView91.check(matches(withText("-10000.00")));

        ViewInteraction textView92 = onView(
                allOf(withId(android.R.id.text1), withText("South"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        22),
                                0),
                        isDisplayed()));
        textView92.check(matches(withText("South")));

        ViewInteraction textView93 = onView(
                allOf(withId(android.R.id.text2), withText("10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        22),
                                1),
                        isDisplayed()));
        textView93.check(matches(withText("10000.00")));

        ViewInteraction textView94 = onView(
                allOf(withId(android.R.id.text1), withText("East"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                0),
                        isDisplayed()));
        textView94.check(matches(withText("East")));

        ViewInteraction textView95 = onView(
                allOf(withId(android.R.id.text2), withText("-10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                1),
                        isDisplayed()));
        textView95.check(matches(withText("-10000.00")));

        ViewInteraction textView96 = onView(
                allOf(withId(android.R.id.text2), withText("-10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        23),
                                1),
                        isDisplayed()));
        textView96.check(matches(withText("-10000.00")));

        ViewInteraction textView97 = onView(
                allOf(withId(android.R.id.text1), withText("West"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        24),
                                0),
                        isDisplayed()));
        textView97.check(matches(withText("West")));

        ViewInteraction textView98 = onView(
                allOf(withId(android.R.id.text2), withText("10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        24),
                                1),
                        isDisplayed()));
        textView98.check(matches(withText("10000.00")));

        ViewInteraction relativeLayout13 = onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.tabs),
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0)),
                        3),
                        isDisplayed()));
        relativeLayout13.perform(click());

        DataInteraction twoLineListItem3 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.content),
                                0)))
                .atPosition(1);
        twoLineListItem3.perform(click());

        ViewInteraction editText34 = onView(
                allOf(withId(getId("title")), withText("Pru"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText34.perform(scrollTo(), click());

        ViewInteraction editText35 = onView(
                allOf(withId(getId("title")), withText("Pru"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText35.perform(scrollTo(), replaceText("Prueba"));

        ViewInteraction editText36 = onView(
                allOf(withId(getId("title")), withText("Prueba"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0),
                        isDisplayed()));
        editText36.perform(closeSoftKeyboard());

        ViewInteraction cursorSpinner4 = onView(
                allOf(withId(getId("type")),
                        childAtPosition(
                                allOf(withId(getId("contents")),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        cursorSpinner4.perform(scrollTo(), click());

        onView(allOf(withId(android.R.id.text1), withText("Car"))).perform(click());

        ViewInteraction button15 = onView(
                allOf(withId(getId("save_btn")), withText("Save changes"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button15.perform(click());

        ViewInteraction textView99 = onView(
                allOf(withId(android.R.id.text1), withText("Prueba"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.list),
                                        1),
                                0),
                        isDisplayed()));
        textView99.check(matches(withText("Prueba")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private static int getId(String id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        String packageName = targetContext.getPackageName();
        return targetContext.getResources().getIdentifier(id, "id", packageName);
    }
}
