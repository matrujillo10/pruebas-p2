package net.cmoaciopm.androidtest;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

public class Test4 {
    private static final String ACTIVITY_TO_BE_TESTED = "com.evancharlton.mileage.Mileage";

    @Before
    public void setup() {
        Class activityClass;
        try {
            activityClass = Class.forName(ACTIVITY_TO_BE_TESTED);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        instrumentation.setInTouchMode(true);

        final String targetPackage = instrumentation.getTargetContext().getPackageName();
        Intent startIntent = new Intent(Intent.ACTION_MAIN);
        startIntent.setClassName(targetPackage, activityClass.getName());
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        instrumentation.startActivitySync(startIntent);
        instrumentation.waitForIdleSync();
    }

    @Test
    public void mileageTest() throws ClassNotFoundException {
        ViewInteraction editText = onView(
                allOf(withId(getId("price")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                0)));
        editText.perform(scrollTo(), replaceText("100"));

        ViewInteraction editText2 = onView(
                allOf(withId(getId("volume")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        0),
                                1)));
        editText2.perform(scrollTo(), replaceText("100"));

        ViewInteraction editText3 = onView(
                allOf(withId(getId("odometer")),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                0)));
        editText3.perform(scrollTo(), replaceText("100"));

        ViewInteraction dateButton = onView(
                allOf(withId(getId("date")), withText("12/10/19"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.TableLayout")),
                                        1),
                                1)));
        dateButton.perform(scrollTo(), click());

        ViewInteraction editText4 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("Dec"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        editText4.perform(replaceText("Nov"));

        ViewInteraction editText5 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("Nov"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                1),
                        isDisplayed()));

        ViewInteraction editText6 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("10"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                1)),
                                1),
                        isDisplayed()));
        editText6.perform(replaceText("20"));

        ViewInteraction editText7 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("20"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                1)),
                                1),
                        isDisplayed()));
        editText7.perform();

        ViewInteraction editText8 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("2019"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                2)),
                                1),
                        isDisplayed()));
        editText8.perform(replaceText("2019"));

        ViewInteraction editText9 = onView(
                allOf(withClassName(is("android.widget.EditText")), withText("2019"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.NumberPicker")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                2)),
                                1),
                        isDisplayed()));
        editText9.perform();

        ViewInteraction button = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                1),
                        isDisplayed()));
        button.perform(click());

        ViewInteraction fieldView = onView(
                childAtPosition(
                        allOf(withId(getId("container")),
                                childAtPosition(
                                        withId(getId("contents")),
                                        4)),
                        1));
        fieldView.perform(scrollTo(), replaceText("fillup test"));

        ViewInteraction editText10 = onView(
                allOf(withId(getId("price")), withText("100"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        0),
                                0),
                        isDisplayed()));
        editText10.check(matches(withText("100")));

        ViewInteraction editText11 = onView(
                allOf(withId(getId("volume")), withText("100"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        0),
                                1),
                        isDisplayed()));
        editText11.check(matches(withText("100")));

        ViewInteraction editText12 = onView(
                allOf(withId(getId("odometer")), withText("100"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                        1),
                                0),
                        isDisplayed()));
        editText12.check(matches(withText("100")));

        ViewInteraction button2 = onView(
                allOf(withId(getId("save_btn")), withText("Save Fillup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        button2.perform(click());

        DataInteraction linearLayout = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)))
                .atPosition(0);
        linearLayout.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(android.R.id.text2), withText("false"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        1),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("false")));

        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.text2), withText("100.00 Gallons"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        2),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("100.00 Gallons")));

        ViewInteraction textView3 = onView(
                allOf(withId(android.R.id.text2), withText("100.00 mi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        3),
                                1),
                        isDisplayed()));
        textView3.check(matches(withText("100.00 mi")));

        ViewInteraction textView4 = onView(
                allOf(withId(android.R.id.text2), withText("$100.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        4),
                                1),
                        isDisplayed()));
        textView4.check(matches(withText("$100.00")));

        ViewInteraction textView5 = onView(
                allOf(withId(android.R.id.text2), withText("$10000.00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(getId("stat_container")),
                                        5),
                                1),
                        isDisplayed()));
        textView5.check(matches(withText("$10000.00")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    public static Matcher<String> matchesRegex(final String regex) {
        return new TypeSafeMatcher<String>() {
            @Override
            public boolean matchesSafely(String item) {
                return item.matches(regex);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("matches regex=`" + regex + "`");
            }
        };
    }

    public static int getId(String id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        String packageName = targetContext.getPackageName();
        return targetContext.getResources().getIdentifier(id, "id", packageName);
    }
}
